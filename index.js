const SitemapGenerator = require('sitemap-generator')
const fileRestrictions = /^.+(\.gif|\.ico|\.jpeg|\.jpg|\.pdf|\.png|\.txt|\.xml)$/
const websiteUrl = 'https://www.codev.com/'
const timestamp = new Date().getTime()

// create generator
const generator = SitemapGenerator(websiteUrl, {
  stripQuerystring: true,
  filepath: null
})

const sanitizeArray = (array) => {
	let sanitizedArray = array.filter(el => !(el === undefined || el === false))
	let result = Array.from(new Set(sanitizedArray))

	return result
}

const fixUrl = (url) => {
  return url.replace(/^((http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/).+)\/.+\..+$/, '$1')
}

// register event listeners
generator.on('done', () => {
  // sitemaps created
  const crawler = generator.getCrawler()
  const crawlerQueue = crawler.queue
  const urls = []

  for(let i=0; i<crawlerQueue.length; i++) {
    let currentQueue = crawlerQueue[i]
    let validUrl = ''

    if(!(currentQueue.url).match(fileRestrictions)) {
        validUrl = (currentQueue.url).replace(/^(.*)\/$/, '$1')
        urls[i] = fixUrl(validUrl)
    }
  }

  console.log(sanitizeArray(urls).toString())
})

// start the crawler
generator.start()